<?php
namespace App\Shell;

use Cake\Console\Shell;

/**
 * LoadPokemons shell command.
 */
class LoadPokemonsShell extends Shell
{
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
      $http = new Client();
    for($index = 1; $index <= 151; $index++){
      $url = "https://pokeapi.co/api/v2/pokemon/" . $index . "/";
      $data = $http->get($url);
      if(!empty($data->getJson())){
          $content = $data->getJson();
          $request = TableRegistry::getTableLocator()->get('Pokemons')->find();
          $request->select(['name'])->where(['name' => $content['forms'][0]['name']]);
          if($request->count() == 0){
            $request = TableRegistry::getTableLocator()->get('Pokemons')->query();
            $request->insert(['id','name','hp','created','modified','attack','defense'])
             ->values(['id' => $index,
             'name' => $content['forms'][0]['name'],
             'hp'=>($data->getJson()['stats'][5]['base_stat'])*100,
             'attack'=>$data->getJson()['stats'][4]['base_stat'],
             'defense' => $data->getJson()['stats'][3]['base_stat'],
             'created' => date("Y-m-d G:i:s"),
             'modified' => date("Y-m-d G:i:s")
             ])
             ->execute();
          }
        }
        else{
          $this->out("No data in URL");
        }
      }
    }
}
