<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fight[]|\Cake\Collection\CollectionInterface $fights
 */
?>
<body class="text-monospace" >
    <div class="row row-cols-4" style="max-width: 90%;">
        <?php foreach ($fights as $fight): ?>
            <div class="card border-dark mb-3" style="max-width: 18rem; min-width: 11rem; margin: 0 auto;">
                <div class="card-header" style="background:#2b73b9;">
                    <h5 class="card-title" style="text-align: center;"> <?= h($fight->id) ?> </h5>
                </div>
                <div class="card-body" >
                    <p> ceci est un fight </p>

                    <p> Dresseur 1: <?= h($fight->first_dresseur_id) ?> </p>
                    <p> Dresseur 2: <?= h($fight->second_dresseur_id) ?> </p>
                    <p> Gagnant : <?= h($fight->winner_dresseur_id) ?> </p>

                </div>
                <div class="card-footer">
                <div class="row row-cols-3 " style="text-align: center;">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $fight->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $fight->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $fight->id], ['confirm' => __('Are you sure you want to delete {0}?', $fight->name)]) ?>
                </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</body>


<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>
