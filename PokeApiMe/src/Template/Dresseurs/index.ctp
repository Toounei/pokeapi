<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur[]|\Cake\Collection\CollectionInterface $dresseurs
 */
?>
<body class="text-monospace" >
    <div class="row row-cols-4" style="max-width: 90%;">
        <?php foreach ($dresseurs as $dresseur): ?>
            <div class="card border-dark mb-3" style="max-width: 18rem; min-width: 11rem; margin: 0 auto;">
                <div class="card-header" style="background:#2b73b9;">
                    <h5 class="card-title" style="text-align: center;"> <?= h($dresseur->name) ?> </h5>
                </div>
                <div class="card-body" >
                    <p> ceci est un dresseur </p>
                </div>
                <div class="card-footer">
                <div class="row row-cols-3 " style="text-align: center;">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dresseur->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dresseur->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dresseur->id], ['confirm' => __('Are you sure you want to delete {0}?', $dresseur->name)]) ?>
                </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</body>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
