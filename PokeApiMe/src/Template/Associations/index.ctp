<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Association[]|\Cake\Collection\CollectionInterface $associations
 */
?>
<body class="text-monospace" >
    <div class="row row-cols-4" style="max-width: 90%;">
        <?php foreach ($associations as $association): ?>
            <div class="card border-dark mb-3" style="max-width: 18rem; min-width: 11rem; margin: 0 auto;">
                <div class="card-header" style="background:#2b73b9;">
                    <h5 class="card-title" style="text-align: center;"> <?= h($association->id) ?> </h5>
                </div>
                <div class="card-body" >
                    <p> ceci est une association </p>

                    <p> Dresseur_id: <?= h($association->dresseur_id) ?> </p>
                    <p> Pokemon_id: <?= h($association->pokemon_id) ?> </p>
                    <p> Nickname: <?= h($association->nickname) ?> </p>
                    <p> isFavorite: <?= h($association->isFavorite) ?> </p>

                </div>
                <div class="card-footer">
                <div class="row row-cols-3 " style="text-align: center;">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $association->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $association->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $association->id], ['confirm' => __('Are you sure you want to delete {0}?', $association->name)]) ?>
                </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</body>

<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>
