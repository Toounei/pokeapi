<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Association $association
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Association'), ['action' => 'edit', $association->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Association'), ['action' => 'delete', $association->id], ['confirm' => __('Are you sure you want to delete # {0}?', $association->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Associations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Association'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pokemons'), ['controller' => 'Pokemons', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pokemon'), ['controller' => 'Pokemons', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="associations view large-9 medium-8 columns content">
    <h3><?= h($association->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Dresseur') ?></th>
            <td><?= $association->has('dresseur') ? $this->Html->link($association->dresseur->name, ['controller' => 'Dresseurs', 'action' => 'view', $association->dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pokemon') ?></th>
            <td><?= $association->has('pokemon') ? $this->Html->link($association->pokemon->name, ['controller' => 'Pokemons', 'action' => 'view', $association->pokemon->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nickname') ?></th>
            <td><?= h($association->nickname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($association->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($association->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($association->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IsFavorite') ?></th>
            <td><?= $association->isFavorite ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
