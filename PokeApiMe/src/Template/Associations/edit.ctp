<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Association $association
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $association->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $association->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Associations'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pokemons'), ['controller' => 'Pokemons', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pokemon'), ['controller' => 'Pokemons', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="associations form large-9 medium-8 columns content">
    <?= $this->Form->create($association) ?>
    <fieldset>
        <legend><?= __('Edit Association') ?></legend>
        <?php
            echo $this->Form->control('dresseur_id', ['options' => $dresseurs]);
            echo $this->Form->control('pokemon_id', ['options' => $pokemons]);
            echo $this->Form->control('nickname');
            echo $this->Form->control('isFavorite');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
