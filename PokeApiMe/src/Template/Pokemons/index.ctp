<script>
function getSprite(PokeNumber){
    $.ajax({
        url: "http://pokeapi.co/api/v2/pokemon/"+ PokeNumber +"/",
        type: "GET",
        dataType: "json",
        success: function(data) {
            console.log("je passe ici");
            $("#img" + PokeNumber).attr("src", data.sprites.front_default)
        }
    });
}
</script>

<body class="text-monospace" >
    <div class="row row-cols-4" style="max-width: 90%;">
        <?php foreach ($pokemon as $poke): ?>
            <div class="card border-dark mb-3" style="max-width: 18rem; min-width: 11rem; margin: 0 auto;">
                <div class="card-header" style="background:#2b73b9;">
                    <h5 class="card-title" style="text-align: center;"> <?= h($poke->name) ?> </h5>
                </div>
                <div class="card-body" >

                  <img id="<?= "img" . $poke->id ?>" class="card-img-top border border-warning"></img>
                  <script>getSprite(<?= $poke->id?>);</script>

                  <p> PokeID: <?= $this->Number->format($poke->id) ?> </p>
                  <p> Hp: <?= $this->Number->format($poke->hp) ?> </p>
                  <p> Attack: <?= $this->Number->format($poke->attack) ?> </p>
                  <p> Defense: <?= $this->Number->format($poke->defense) ?> </p>
                  </div>
                  <div class="card-footer">
                    <div class="row row-cols-3 " style="text-align: center;">
                      <?= $this->Html->link(__('View'), ['action' => 'view', $poke->id]) ?>
                      <?= $this->Html->link(__('Edit'), ['action' => 'edit', $poke->id]) ?>
                      <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $poke->id], ['confirm' => __('Are you sure you want to delete {0}?', $poke->name)]) ?>
                    </div>
                  </div>
            </div>
        <?php endforeach; ?>
      </div>
</body>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>
