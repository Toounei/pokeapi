<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pokemon $pokemon
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pokemon'), ['action' => 'edit', $pokemon->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pokemon'), ['action' => 'delete', $pokemon->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pokemon->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pokemons'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pokemon'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pokemons view large-9 medium-8 columns content">
    <h3><?= h($pokemon->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($pokemon->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($pokemon->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hp') ?></th>
            <td><?= $this->Number->format($pokemon->hp) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Attack') ?></th>
            <td><?= $this->Number->format($pokemon->attack) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Defense') ?></th>
            <td><?= $this->Number->format($pokemon->defense) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($pokemon->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($pokemon->modified) ?></td>
        </tr>
    </table>
</div>
