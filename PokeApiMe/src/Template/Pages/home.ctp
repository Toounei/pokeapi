<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->layout = false;

if (!Configure::read('debug')) :
    throw new NotFoundException(
        'Please replace src/Template/Pages/home.ctp with your own version or re-enable debug mode.'
    );
endif;

$cakeDescription = 'CakePHP: the rapid development PHP framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('home.css') ?>
<style>
    .module {
        display: grid;
        grid-template-columns: repeat(2,1fr);
        grid-gap: 10px;
        grid-auto-rows: minmax(10px,auto);
        margin-top: 20px;
        margin-bottom: 20px;
        padding-top: 10px;
        margin: 0 auto;
    }
    .post{
        margin: 0 auto;
        border-radius: 3px;
        background-color: #ff000020;
        text-align: center;
        box-shadow: 0 1px 4px
        rgba(255,62,80,0.2);
        display: flex;
        justify-content: center;
        align-items: center;
        height: 150px;
        max-width: 90%;
    }

</style>
</head>
<body>
    <header >
        <div class="header-image"><?= $this->Html->image('https://upload.wikimedia.org/wikipedia/commons/9/98/International_Pokémon_logo.svg') ?></div>
        <div class="header-title"><h1>Bienvenue sur la PokeApi de Valentin Le Bastard </h1></div>
    </header>
    <main>
        <div class="container">
            <div class="row row-cols-2 module">
                <div class="col post"><?= $this->Html->link(__('Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></div>
                <div class="col post"><?= $this->Html->link(__('Pokemons'), ['controller' => 'Pokemons', 'action' => 'index']) ?></div>
                <div class="col post"><?= $this->Html->link(__('Associations'), ['controller' => 'Associations', 'action' => 'index']) ?></div>
                <div class="col post"><?= $this->Html->link(__('Fights'), ['controller' => 'Fights', 'action' => 'index']) ?></div>
            </div>
        </div>
    </main>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
