<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Associations Controller
 *
 * @property \App\Model\Table\AssociationsTable $Associations
 *
 * @method \App\Model\Entity\Association[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AssociationsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dresseurs', 'Pokemons']
        ];
        $associations = $this->paginate($this->Associations);

        $this->set(compact('associations'));
    }

    /**
     * View method
     *
     * @param string|null $id Association id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $association = $this->Associations->get($id, [
            'contain' => ['Dresseurs', 'Pokemon']
        ]);

        $this->set('association', $association);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $association = $this->Associations->newEntity();
        if ($this->request->is('post')) {
            $association = $this->Associations->patchEntity($association, $this->request->getData());
            if ($this->Associations->save($association)) {
                $this->Flash->success(__('The association has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The association could not be saved. Please, try again.'));
        }
        $dresseurs = $this->Associations->Dresseurs->find('list', ['limit' => 200]);
        $pokemon = $this->Associations->Pokemons->find('list', ['limit' => 200]);
        $this->set(compact('association', 'dresseurs', 'pokemon'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Association id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $association = $this->Associations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $association = $this->Associations->patchEntity($association, $this->request->getData());
            if ($this->Associations->save($association)) {
                $this->Flash->success(__('The association has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The association could not be saved. Please, try again.'));
        }
        $dresseurs = $this->Associations->Dresseurs->find('list', ['limit' => 200]);
        $pokemon = $this->Associations->Pokemons->find('list', ['limit' => 200]);
        $this->set(compact('association', 'dresseurs', 'pokemon'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Association id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $association = $this->Associations->get($id);
        if ($this->Associations->delete($association)) {
            $this->Flash->success(__('The association has been deleted.'));
        } else {
            $this->Flash->error(__('The association could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
