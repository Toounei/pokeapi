<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Fights Controller
 *
 * @property \App\Model\Table\FightsTable $Fights
 *
 * @method \App\Model\Entity\Fight[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FightsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ];
        $fights = $this->paginate($this->Fights);

        $this->set(compact('fights'));
    }

    /**
     * View method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ]);

        $this->set('fight', $fight);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fight = $this->Fights->newEntity();
        if ($this->request->is('post')) {
            $formData = $this->request->getData();
            $formData['winner_dresseur_id'] = $this->_retrieveFightWinner($formData);
debug($formData);
            if($formData['winner_dresseur_id']){
              debug($formData['winner_dresseur_id']);
                $fight = $this->Fights->patchEntity($fight, $formData);
                if ($this->Fights->save($fight)) {
                    $this->Flash->success(('The fight has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }
            }

            $this->Flash->error(('The fight could not be saved. Please, try again.'));
        }
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list');
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list');
        $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs'));
    }

    protected function _retrieveFightWinner($data){
        //Pokemon dressseur 1
        $associations = TableRegistry::getTableLocator()->get('Associations');
        $query = $associations->find()
                    ->select(['pokemon_id'])
                    ->where(['dresseur_id =' => $data['first_dresseur_id']])
                    ->where(['isFavorite =' => True]);
        foreach ($query as $article) {$idpokedresseur1 = $article->pokemon_id;}
        $pokemons = TableRegistry::getTableLocator()->get('Pokemons');
        $query = $pokemons->find()
                    ->where(['id = ' => $idpokedresseur1]);
        foreach ($query as $article) {$poke1 = $article;}
        $first_dresseur = ['id'=>$data['first_dresseur_id'],'poke'=>$poke1];
        debug($first_dresseur);
        //Pokemon dressseur 2
        $associations = TableRegistry::getTableLocator()->get('Associations');
        $query = $associations->find()
                    ->select(['pokemon_id'])
                    ->where(['dresseur_id =' => $data['second_dresseur_id']])
                    ->where(['isFavorite =' => True]);
        foreach ($query as $article) {$idpokedresseur2 = $article->pokemon_id;}

        $pokemons = TableRegistry::getTableLocator()->get('Pokemons');
        $query = $pokemons->find()
                    ->where(['id = ' => $idpokedresseur2]);
        foreach ($query as $article) {$poke2 = $article;}
        $second_dresseur = ['id'=>$data['second_dresseur_id'],'poke'=>$poke2];
        debug($second_dresseur);

        return $this->fight($first_dresseur['poke'], $second_dresseur['poke'], $first_dresseur['id'], $second_dresseur['id']);
    }

    protected function fight($pokemon1, $pokemon2, $dresseur1, $dresseur2){
        $pokemon1->hp = ($pokemon2->attack - $pokemon1->defense >= 1) ? $pokemon1->hp - ($pokemon2->attack*(1 + (rand(-10, 10)/100)) - $poke1->defense) : $pokemon1->hp - 1;
        $pokemon2->hp = ($pokemon1->attack - $pokemon2->defense >= 1) ? $pokemon2->hp - ($pokemon1->attack*(1 + (rand(-10, 10)/100)) - $pokemon2->defense) : $pokemon2->hp - 1;
        if($pokemon1->hp > 0 && $pokemon2->hp > 0){return $this->fight($pokemon1, $pokemon2, $dresseur1, $dresseur2);}
        else{
            if($pokemon1->hp <= 0 && $pokemon2->hp <= 0){
                $random = rand(1, 100);
                if($random > 50){return $dresseur1;}
                else{return $dresseur2;}
            }
            if($pokemon1->hp <= 0){return $dresseur2;}
            if($pokemon2->hp <= 0){return $dresseur1;}
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fight = $this->Fights->get($id);
        if ($this->Fights->delete($fight)) {
            $this->Flash->success(__('The fight has been deleted.'));
        } else {
            $this->Flash->error(__('The fight could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
