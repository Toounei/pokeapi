<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Associations Model
 *
 * @property \App\Model\Table\DresseursTable&\Cake\ORM\Association\BelongsTo $Dresseurs
 * @property \App\Model\Table\PokemonTable&\Cake\ORM\Association\BelongsTo $Pokemon
 *
 * @method \App\Model\Entity\Association get($primaryKey, $options = [])
 * @method \App\Model\Entity\Association newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Association[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Association|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Association saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Association patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Association[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Association findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AssociationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('associations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Dresseurs', [
            'foreignKey' => 'dresseur_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Pokemons', [
            'foreignKey' => 'pokemon_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('nickname')
            ->maxLength('nickname', 255)
            ->requirePresence('nickname', 'create')
            ->notEmptyString('nickname');

        $validator
            ->boolean('isFavorite')
            ->requirePresence('isFavorite', 'create')
            ->notEmptyString('isFavorite');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['dresseur_id'], 'Dresseurs'));
        $rules->add($rules->existsIn(['pokemon_id'], 'Pokemons'));

        return $rules;
    }
}
